package aelb.carto.qgis.utils;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SendMailUtils {

	private static Logger logger = LogManager.getLogger(SendMailUtils.class.getName());

	public static boolean sendMail(String username, String qgisProject, String projectUrl) throws Exception {

		ConfigurationManager config = ConfigurationManager.getInstance();
		String to = config.getString("mail.adminMail");
		String from = config.getString("mail.fromMail");
		String host = config.getString("mail.smtpHost");
		String user = config.getString("mail.smtpUser");
		String password = config.getString("mail.smtpPassword");
		String messageTemplate = config.getString("mail.messageTemplate");

		Session session;

		Properties prop = new Properties();

		prop.put("mail.transprt.protocol", "smtp");
		prop.put("mail.smtp.host", host);

		if (user != null && password != null) {
			prop.put("mail.smtp.auth", true);

			Authenticator auth = new SMTPAuthenticator();
			session = Session.getDefaultInstance(prop, auth);
		} else {
			session = Session.getDefaultInstance(prop);
		}

		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			Address[] cc = new Address[] {};
			message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(to));

			// Set Subject: header field
			message.setSubject(config.getString("mail.object"));

			String txtMessage = String.format(messageTemplate, username, qgisProject, projectUrl);

			// Now set the actual message
			message.setText(txtMessage);

			// Send message
			Transport.send(message);
			logger.info("Sent message successfully.... : " + username + "/" + qgisProject + "/" + projectUrl);
		} catch (MessagingException e) {
			logger.error("Error while sending email", e);
			return false;
		}

		return true;
	}
}


