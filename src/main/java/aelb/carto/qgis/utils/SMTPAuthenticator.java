package aelb.carto.qgis.utils;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SMTPAuthenticator extends Authenticator {

	private static Logger logger = LogManager.getLogger(SMTPAuthenticator.class.getName());

	public PasswordAuthentication getPasswordAuthentication() {

		String username = null;
		String password = null;
		try {
			username = ConfigurationManager.getInstance().getString("mail.user");
			password = ConfigurationManager.getInstance().getString("mail.password");
		} catch (Exception e) {
			logger.error("Error getting mail user/password params", e);
		}
		return new PasswordAuthentication(username, password);
	}
}
