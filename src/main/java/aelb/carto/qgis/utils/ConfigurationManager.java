package aelb.carto.qgis.utils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;

public class ConfigurationManager {

	// Variables de gestion du singleton
	private static Map<String, ConfigurationManager> mapConfigs = new HashMap<>();
	
	//Configuration associee a l'instance du ConfigManager
	private Configuration config;
	
	/**
	 * Permet de recuperer un instance de ConfigurationManager base sur le fichier properties config.properties par defaut.
	 * @return
	 * @throws Exception
	 */
	public static ConfigurationManager getInstance() throws Exception{
		return getInstance("/config.properties");
	}
	
	/**
	 * Permet de recuperer une instance de ConfigurationManager base sur le fichier properties passe en parametre
	 * @param configFile
	 * @return
	 * @throws Exception
	 */
	public static ConfigurationManager getInstance(String configFile) throws Exception{
		//Si l'instnace n'existe pas pour ce fichier de config, on le cree.
		if(!mapConfigs.containsKey(configFile)){
			Configurations configs = new Configurations();
			try {
				File configurationFile = new File(configFile);
				Configuration config = configs.properties(configurationFile);
				ConfigurationManager configManager = new ConfigurationManager(config);
				mapConfigs.put(configFile, configManager);
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		}
		return mapConfigs.get(configFile);
	}
	
	private ConfigurationManager(Configuration config) throws Exception{
		this.config = config;
	}
	
	public String getString(String key){
		return config.getString(key);
	}
}
