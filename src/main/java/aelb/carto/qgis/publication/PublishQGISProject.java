package aelb.carto.qgis.publication;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.Pattern;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import aelb.carto.qgis.utils.ConfigurationManager;
import aelb.carto.qgis.utils.SendMailUtils;

@Path("publishQGSIProject")
public class PublishQGISProject {

	private Logger logger = LogManager.getLogger(PublishQGISProject.class.getName());

	@POST
	@Path("upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(@FormDataParam("file") InputStream fileInputStream,
			@FormDataParam("file") FormDataContentDisposition contentDispositionHeader,
			@FormDataParam("username") String username, @FormDataParam("overwrite") String overwrite) throws Exception {
		logger.info("Call to uploadFile");
		String projectPath = ConfigurationManager.getInstance().getString("projectPath");
		

		// Add username and add username to filePath
		// projectPath/username/fileName.qgs
		String output = "";
		String filename = contentDispositionHeader.getFileName();
		if (username == null || username.isEmpty() || contentDispositionHeader == null || filename == null
				|| filename.isEmpty() || projectPath == null || projectPath.isEmpty()) {
			output = "missingParameters";
			logger.error("path = " + projectPath);
			logger.error("Username = " + username);
			logger.error("Filename = " + contentDispositionHeader);
			return Response.status(200).entity(output).build();
		}
		
		String fileDirectory = projectPath + File.separator + username;
		String filePath = projectPath + File.separator + username + File.separator
				+ filename;

		logger.debug("path = " + filePath);
		logger.debug("Username = " + username);
		logger.debug("Filename = " + filename);

		File folder = new File(fileDirectory);

		createFolderIfNotExists(fileDirectory, folder);

		File file = new File(filePath);

		if (file.exists()) {
			logger.warn("File Already Exists : " + filePath);
			Boolean overwriteBoolean = Boolean.parseBoolean(overwrite);
			if (overwriteBoolean) {
				file.delete();
				saveFile(fileInputStream, filePath);
				logger.info("Project overwrited");
			} else {
				output = "Exist";
				return Response.status(200).entity(output).build();
			}

		} else {
			saveFile(fileInputStream, filePath);
			logger.info("File saved to server location : " + filePath);

			output = "Success";

		}
		sendMail(username, filename);
		return Response.status(200).entity(output).build();

	}

	private void createFolderIfNotExists(String fileDirectory, File folder) {
		if (!folder.exists() || !folder.isDirectory()) {
			logger.warn("Directory Doesnt Exists : " + fileDirectory);
			try {
				folder.mkdirs();
			} catch (SecurityException e1) {
				logger.error("Error while mkdirs", e1);
				throw e1;
			}
		}
	}

	private void sendMail(String username, String filename) throws Exception {
		String serverHost = ConfigurationManager.getInstance().getString("server.host");
		String[] filenameParts = filename.split(Pattern.quote("."));
		
		Integer len = filenameParts.length;
		String extension="." + filenameParts[len-1];
		logger.debug("extension " + extension);
		String filenameWithoutExtension = filename.replace(extension, "");
		logger.debug("filenameWithoutExtension " + filenameWithoutExtension);
		String projectUrl = serverHost + "/" + "wms" + "/" + username + "/" + filenameWithoutExtension + "?service=WMS&version=1.3.0&request=getCapabilities";
		SendMailUtils.sendMail(username, filename, projectUrl);
	}

	@GET
	@Path("authorizedDomains")
	public Response getAuthorizedDomains() throws Exception {
		String domains = ConfigurationManager.getInstance().getString("authorizedDomainsConf");
		return Response.status(200).entity(domains).build();

	}

	@GET
	@Path("authorizedDatabase")
	public Response authorizedDatabase() throws Exception {
		// Db host
		String db = ConfigurationManager.getInstance().getString("authorizedDatabasesConf");
		return Response.status(200).entity(db).build();

	}


	private void saveFile(InputStream fileInputStream, String filePath) {
		try {
			OutputStream outputStream = new FileOutputStream(new File(filePath));
			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = fileInputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
			outputStream.flush();
			outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
