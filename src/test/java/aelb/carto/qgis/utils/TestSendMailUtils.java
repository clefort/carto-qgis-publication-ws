package aelb.carto.qgis.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class TestSendMailUtils {

	@Test public void sendMail(){
		boolean result;
		try {
			result = SendMailUtils.sendMail("user1", "project1");
		} catch (Exception e) {
			result = false;
		}
		assertEquals(Boolean.TRUE, result);
	}
}
