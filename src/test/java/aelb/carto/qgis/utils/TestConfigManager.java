package aelb.carto.qgis.utils;
import static org.junit.Assert.*;

import org.junit.Test;

public class TestConfigManager {
	@Test public void testConfigManagerInit() throws Exception{
		ConfigurationManager instance = ConfigurationManager.getInstance("/testConfig.properties");
		assertNotNull(instance);
	}
	
	@Test public void testConfigManagerString() throws Exception{
		String testValue = ConfigurationManager.getInstance("/testConfig.properties").getString("testKey");
		assertNotNull(testValue);
		assertEquals("testValue", testValue);
	}
}
