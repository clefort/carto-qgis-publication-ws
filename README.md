Webservice carto-qgis-publication-ws

carto-qgis-publication-ws est une webapp Java basée sur Jersey exposant des Webservices REST dédiés à la publication de projet QGIS sur QGIS Server.

Ce webservice a été développé dans le cadre de la refonte du SIG de l’Agence de l’eau Loire-Bretagne (http://www.eau-loire-bretagne.fr) s’appuyant sur l’utilisation de logiciels libres et ayant pour ambition de faciliter et rationaliser l’acquisition, la valorisation, la diffusion et la présentation de l’information géographique. 
Il accompagne le plugin GIS (qgis-publication-plugin https://bitbucket.org/clefort/carto-qgis-publication-plugin).

L’objectif de ce plugin est de faciliter la publication de projets cartographiques QGIS Desktop au sein de l’IDS et d’en conserver le plus fidèlement possible le langage cartographique. Ainsi, une carte et les couches qui la composent peuvent être poussées sur QGIS Server qui en propose la publication en Web service WMS. L’adresse de ce WMS est communiquée par courriel, il est alors possible de la référencer dans une application de Web Mapping ou en tant que WMS externe (WMS cascading) sur un autre serveur cartographique.
Le développement du plugin et du webservice a été financé par l’Agence de l’eau Loire-Bretagne et réalisé par l’entreprise de services du numérique Capgemini (http://www.fr.capgemini.com/).

L’Agence de l’eau, établissement public français, s’engage depuis plus de 50 ans aux côtés des élus et des usagers de l’eau pour la qualité de l’eau et des milieux aquatiques.

La configuration du webservice est à réaliser dans le fichier src\main\resources\config.properties.

Ce développement est diffusé sous licence GNU GPL 3 (https://www.gnu.org/licenses/gpl-3.0.fr.html), sans garantie et sans support utilisateur

Contact :
francois.lyonnais@eau-loire-bretagne.fr


Notes de développement :
========================
Prérequis
------------
Il est nécessaire d'avoir sur son poste :
* Eclipse avec les plugins
  * Eclipse Java EE Developer Tools
  * Eclipse Java Web Developer Tools
  * JST Server Adapters
  * JST Server Adapters Extensions
  * JST Server UI
* Java 8 SDK
* Gradle

Pour ajouter les plugins dans Eclipse :
* Ouvrir eclipse Neon
* Help > Add new software
* Selectionner la source "Neon"
* Selectionner les packages précédents dans "Web, XML, Java EE and OSGi"

Installation
------------
* git clone [URL]
* cd carto-ws-publication-qgis
* gradlew eclipse
* Ouvrir eclipse dans un workspace
* Importer le projet dans le workspace